#!/bin/bash

set -e

echo "Configuring Workbench"

sleep 5

# Update the docker containers and install common packages
for i in workbench; do docker exec -i $i bash -c "sudo apt-get update -y -qq ; sudo apt-get upgrade -y -qq ; sudo apt-get install wget apt-transport-https curl -y"; done

echo "Configuring Workbench now..."

# Install virt-manager
docker exec workbench sudo sh -c "sudo apt-get -y -qq install qemu qemu-utils qemu-kvm libvirt-daemon-system libvirt-clients bridge-utils synaptic virt-manager"

# Install Terraform AMD64
docker exec workbench sudo sh -c "sudo wget -O terraform-amd64.zip https://releases.hashicorp.com/terraform/1.1.7/terraform_1.1.7_linux_amd64.zip && sudo unzip terraform-amd64.zip && sudo mv terraform usr/local/bin ; sudo touch /root/.bashrc && sudo terraform -install-autocomplete"

# Install Github Desktop AMD64
docker exec workbench sudo bash -c 'wget -qO - https://mirror.mwt.me/ghd/gpgkey | tee /etc/apt/trusted.gpg.d/shiftkey-desktop.asc > /dev/null ; echo "deb [arch=amd64] https://mirror.mwt.me/ghd/deb/ any main" > /etc/apt/sources.list.d/packagecloud-shiftkey-desktop.list'

docker exec workbench sudo sh -c 'apt-get update -y && sudo apt-get install github-desktop -y'

echo "Configuring Security-Operation-Center now..."

sleep 5

docker exec Security-Operation-Center sh -c 'sudo apk update ; sudo apk upgrade ; sudo apk add libnotify xdg-utils wget dpkg'

echo "Completed."
