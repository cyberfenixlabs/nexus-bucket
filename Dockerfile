FROM alpine:latest
COPY workbench.sh /workbench.sh
RUN chmod +x /workbench.sh
ENTRYPOINT [ "./workbench.sh" ]
CMD "./workbench.sh"
